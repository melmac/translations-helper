package ru.dreendex.translationshelper;

import ru.dreendex.translationshelper.lang.LangManager;
import ru.dreendex.translationshelper.lang.Message;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreeSelectionModel;
import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Comparator;

public class TranslatorHelper {

    private Form form;
    private JFrame frame;
    private JTree tree;
    private File workingDir;
    private LangManager langMgr;
    private LangTreeNode selectedNode;
    private Message selectedMessage;

    public TranslatorHelper() {
        this.workingDir = new File(".");
        this.form = new Form();
        this.frame = new JFrame("MineLand Translator Helper");
        this.langMgr = new LangManager();
    }

    public void start() {
        JMenuBar menuBar = new JMenuBar();

        JMenu menu = new JMenu("File");
        menu.setMnemonic(KeyEvent.VK_F);
        menu.getAccessibleContext().setAccessibleDescription("The only menu in this program that has menu items");
        menuBar.add(menu);

        JMenuItem menuItem = new JMenuItem("Reload", KeyEvent.VK_T);
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_DOWN_MASK));
        menuItem.getAccessibleContext().setAccessibleDescription("Reloads the whole repository");
        menuItem.addActionListener(event -> {
            loadLanguages();
        });
        menu.add(menuItem);

        this.frame.setJMenuBar(menuBar);

        this.frame.setContentPane(form.getContentPanel());
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.frame.setSize(800, 500);
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        this.frame.setLocation(screen.width / 2 - frame.getWidth() / 2, screen.height / 2 - frame.getHeight() / 2);
        this.form.getStatusLabel().setText("Loading...");
        this.form.getSaveButton().setEnabled(false);
        tree = this.form.getTree();
        tree.removeAll();
        tree.setCellRenderer(new TranslationsTreeRenderer(this.langMgr));
        tree.setRootVisible(false);
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        this.frame.setVisible(true);

        this.form.getTree().addTreeSelectionListener(event -> {
            form.getDefaultTranslation().setText("");
            form.getCurrentTranslation().setText("");
            this.selectedNode = null;
            this.selectedMessage = null;
            if (event.getNewLeadSelectionPath() != null) {
                Object obj = event.getNewLeadSelectionPath().getLastPathComponent();
                if (obj instanceof LangTreeNode) {
                    LangTreeNode node = (LangTreeNode) obj;
                    this.selectedNode = node;
                    if (node.getMessage() != null) {
                        Message message = node.getMessage();
                        this.selectedMessage = message;
                        form.getDefaultTranslation().setText(message.getDefaultTranslation());
                        form.getCurrentTranslation().setText(message.getTranslation(node.getLang()));
                    }
                }
            }
            updateSaveButton();
            if (selectedNode != null) {
                System.out.println("PATH SELECTED: " + selectedNode.toString());
                form.getStatusLabel().setText(selectedNode.getFullPath().replace(".", " > "));
            }
        });
        this.form.getSaveButton().addActionListener(event -> {
            langMgr.setAndSave(this.workingDir, this.selectedNode.getLang(), this.selectedMessage, form.getCurrentTranslation().getText());
        });

        loadLanguages();
    }

    private void updateSaveButton() {
        if (selectedMessage != null) {
            form.getSaveButton().setEnabled(true);
        } else {
            form.getSaveButton().setEnabled(false);
        }
    }

    private void loadLanguages() {
        this.langMgr.loadLanguages(this.workingDir);

        DefaultMutableTreeNode languages = new DefaultMutableTreeNode("MinelandTranslations");
        DefaultTreeModel model = new DefaultTreeModel(languages);

        for (String lang : this.langMgr.getLanguages()) {
            LangTreeNode langNode = new LangTreeNode(lang);

            langMgr.getMessages().stream()
                    .sorted(Comparator.naturalOrder())
                    .forEach(message -> createMessageNode(langNode, message));

            languages.add(langNode);
        }

        this.tree.setModel(model);
        this.form.getStatusLabel().setText("Done");
    }

    private LangTreeNode createMessageNode(LangTreeNode node, Message message) {
        String[] a = message.getKey().split("\\.");
        for (String s : a) {
            LangTreeNode node1 = findNode(node, s, message);
            node.add(node1);
            node1.setParent(node);
            node = node1;
        }
        return node;
    }

    private LangTreeNode findNode(LangTreeNode node, String name, Message message) {
        for (int i = 0; i < node.getChildCount(); i++) {
            TreeNode treeNode = node.getChildAt(i);
            if (treeNode instanceof LangTreeNode && treeNode.toString().equals(name)) {
                return (LangTreeNode) treeNode;
            }
        }
        return message.getKey().endsWith("." + name)
                ? new LangTreeNode(node.getLang(), message)
                : new LangTreeNode(node.getLang(), name);
    }
}
