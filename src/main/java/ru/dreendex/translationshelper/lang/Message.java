package ru.dreendex.translationshelper.lang;

import java.util.HashMap;
import java.util.Map;

public class Message implements Comparable<Message> {

    private String key;
    private String defaultTranslation;
    private Map<String, String> translations; // lang, translation

    public Message(String key) {
        this.key = key;
        this.translations = new HashMap<>();
    }

    public String getKey() {
        return key;
    }

    public String getTranslation(String lang) {
        return translations.get(lang);
    }

    public boolean hasLang(String lang) {
        String translation = translations.get(lang);
        if (translation != null && (!translation.equals(getDefaultTranslation()) || lang.equals("en"))) {
            return true;
        }
        return false;
    }

    public void putTranslation(String lang, String translation) {
        if (this.defaultTranslation == null || lang.equals("en")) {
            this.defaultTranslation = translation;
        }
        translations.put(lang, translation);
    }

    public String getDefaultTranslation() {
        return defaultTranslation;
    }

    @Override
    public int compareTo(Message o) {
        return key.compareTo(o.key);
    }
}